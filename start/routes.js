'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.group(() => {
  // login
  Route.post('login/sign-in', 'UserController.login')
  Route.post('login/sign-up', 'UserController.store')

  // product
  Route.get('product', 'producController.index')
  Route.post('product', 'producController.create')
  Route.delete('product/:id', 'producController.destroy')
  Route.patch('product/:id', 'producController.update')
}).prefix('api/v1/')

Route.any('*', 'NuxtController.render')
8
