'use strict'

/*
|--------------------------------------------------------------------------
| ColorSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Color = use('App/Models/Color')

class ColorSeeder {
  async run() {
    const yellow = await Color.create({
      name: 'Yellow',
      hexadecimal: '#FFFF00'
    })
    const blue = await Color.create({
      name: 'Blue',
      hexadecimal: '#0000FF'
    })
    const red = await Color.create({
      name: 'Red',
      hexadecimal: '#FF0000'
    })
  }
}

module.exports = ColorSeeder
