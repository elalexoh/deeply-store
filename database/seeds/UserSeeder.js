'use strict'

/*
|--------------------------------------------------------------------------
| UserSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Database = use('Database')
const User = use('App/Models/User')

class UserSeeder {
  async run() {
    const user = await User.create({
      username: 'g7.alexander@gmail.com',
      email: 'g7.alexander@gmail.com',
      password: 'deeply'
    })
  }
}

module.exports = UserSeeder
