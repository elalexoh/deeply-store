'use strict'

/*
|--------------------------------------------------------------------------
| ProductSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Product = use('App/Models/Product')

class ProductSeeder {
  async run() {
    // const product = await Product.create({
    //   name: 'YT IZZO',
    //   description:
    //     'Inspired by the katana swords of feudal Japan’s samurai – considered to be perfect weapons – the all-new IZZO is our fastest and most agile trail bike ever.',
    //   stock: '1000',
    //   precio: '1500.00',
    //   image: 'http://placehold.it/500',
    //   user_id: 1,
    //   color_id: 1
    // })
  }
}

module.exports = ProductSeeder
