'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ColorsSchema extends Schema {
  up() {
    this.create('colors', (table) => {
      table.increments()
      table.string('name', 100)
      table.string('hexadecimal', 10)
      table.timestamps()
    })
  }

  down() {
    this.drop('colors')
  }
}

module.exports = ColorsSchema
