'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductSchema extends Schema {
  up() {
    this.create('products', (table) => {
      table.increments()
      table.string('name', 80).notNullable()
      table.string('description', 250)
      table.integer('stock', 250)
      table.float('precio', 250)
      table.string('image', 250)
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
      table
        .integer('color_id')
        .unsigned()
        .references('id')
        .inTable('colors')
      table.timestamps()
    })
  }

  down() {
    this.drop('products')
  }
}

module.exports = ProductSchema
