'use strict'

class ProductController {
  index() {
    return 'do u call index'
  }
  create() {
    return 'do u call create'
  }
  destroy() {
    return 'do u call destroy'
  }
  update() {
    return 'do u call update'
  }
}

module.exports = ProductController
