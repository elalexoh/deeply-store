'use strict'
const User = use('App/Models/User')

class UserController {
  async login({ request, auth }) {
    const { email, password } = request.all()
    const token = await auth.attempt(email, password)
    return token
  }
  async store({ request }) {
    // get data
    const { email, password } = request.all()
    //make new user
    const user = await User.create({
      username: email,
      email,
      password
    })
    return user
  }
}

module.exports = UserController
