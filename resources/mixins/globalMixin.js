import { mapActions, mapGetters } from 'vuex'
export default {
  computed: {
    ...mapGetters({
      getMiniVariant: 'ui/getMiniVariant'
    })
  },
  methods: {
    ...mapActions({
      toggleMiniVariant: 'ui/toggleMiniVariant'
    })
  }
}
