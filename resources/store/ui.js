const initialState = () => ({
  miniVariant: true
})

// State object.
const state = initialState

// Getter functions.
const getters = {
  getMiniVariant(state) {
    return state.miniVariant
  }
}

// Actions.
const actions = {
  // GET ALL affilate
  toggleMiniVariant({ commit }) {
    commit('UPDATE_MINI_VARIANT')
  }
}

// Mutations
const mutations = {
  UPDATE_MINI_VARIANT(state) {
    state.miniVariant = !state.miniVariant
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
